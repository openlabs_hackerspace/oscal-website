const eleventySass = require("@11tyrocks/eleventy-plugin-sass-lightningcss");
const {
  EleventyHtmlBasePlugin,
  EleventyRenderPlugin,
} = require("@11ty/eleventy");
const rssPlugin = require("@11ty/eleventy-plugin-rss");

module.exports = (config) => {
  config.addPlugin(eleventySass);

  config.addFilter("getAllTags", (collection) => {
    let tagSet = new Set();
    for (let item of collection) {
      (item.data.tags || []).forEach((tag) => tagSet.add(tag));
    }
    return Array.from(tagSet);
  });

  config.addFilter("filterTagList", function filterTagList(tags) {
    return (tags || []).filter(
      (tag) => ["all", "nav", "post", "posts"].indexOf(tag) === -1
    );
  });

  config.addFilter("svgUrl", (filename) => `./src/_includes${filename}.njk`);

  config.addFilter("formatDate", (date) => {
    const options = { year: "numeric", day: "2-digit", month: "2-digit" };
    return date.toLocaleDateString("de-DE", options);
  });

  config.addPassthroughCopy("./src/assets");
  config.addPassthroughCopy("./src/styles");
  config.addPassthroughCopy("./src/scripts");

  config.addPlugin(EleventyHtmlBasePlugin);
  config.addPlugin(EleventyRenderPlugin);
  config.addPlugin(rssPlugin);

  config.addCollection("posts", (collection) => {
    return collection.getFilteredByGlob("./src/posts/*");
  });

  config.setFrontMatterParsingOptions({
    excerpt: true,
    excerpt_separator: "<!-- excerpt -->",
  });

  return {
    markdownTemplateEngine: "njk",
    dataTemplateEngine: "njk",
    htmlTemplateEngine: "njk",

    dir: {
      input: "src",
      output: "public",
    },
  };
};
