# OSCAL 2023 website

This is the official repo of the website for OSCAL 2023.

## About

OSCAL (Open Source Conference Albania) is the first annual conference in Albania organized to promote software freedom, open source software, free culture and open knowledge. The Conference gathers free libre open source technology users, developers, academics, governmental agencies and people who share the idea that software should be free and open for the local community and governments to develop and customize to its needs; that knowledge is a communal property and free and open to everyone.

## Local development

Clone the repo locally

```
git clone [repo url]
```

Install dependencies (requires node version > 14)

```
npm install
```

Start server

```
npm start
```

License

Source code is published under an [AGPL v3](https://www.gnu.org/licenses/agpl-3.0.html) license. Feel free to clone and modify repo as you want, but don't forget to add reference to the authors and share with the same license :)
