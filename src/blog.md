---
layout: "layouts/pagewide.html"
title: "Blog"
---

<h1 id="all-blogs">All blog posts</h1>

<ul class="all-posts grid" role="list" aria-labelledby="all-blogs" style="--grid-gap: 2em; --minimum: 35ch;">
  {% for post in collections.posts | reverse %}
  <li>
    {% include "partials/post-card.html" %}
  </li>
  {% endfor %}
</ul>
