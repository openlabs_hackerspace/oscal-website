---
title: "Celebrating Diversity and Inclusion at OSCAL 2023"
date: 2023-08-30 10:00:00
image: "/assets/images/diversity"
permalink: "/blog/diversity-and-inclusion-at-oscal/"
imageAlt: "Hands of humans of different colors and genders placed on a table"
---

Today, we proudly announce the inclusion of diversity support initiatives at this year’s Open Source Conference Albania (OSCAL) 2023. OSCAL, the annual volunteer-run event in Albania that celebrates free open-source technologies, online privacy, and open knowledge, will take place at Ambasada e Paqes during the first weekend of September.

## Supporting Women in STEM

The <a href="https://www.linkedin.com/company/network-of-albanian-women-in-stem/mycompany/?viewAsMember=true" target="_blank">Network of Albanian Women in STEM</a> (NAWSTEM) has been doing remarkable work in empowering women in STEM fields. To demonstrate our support for this vital cause, OSCAL is offering 5 free tickets exclusively for members of the network. We wholeheartedly believe in providing opportunities for women to thrive and excel in the tech world. This is an invitation for women in STEM to come, connect, learn, and contribute to the vibrant discussions that OSCAL has to offer.

## Expanding Access to the event

We recognize that financial constraints can sometimes impede enthusiastic minds from participating in events that can shape their personal development. OSCAL is dedicated to dismantling these barriers, one step at a time. That’s why we are giving away 20 tickets to individuals who are unemployed or facing financial difficulties. Your financial situation should not hinder your growth, and we want to ensure that everyone has a chance to attend and engage with the community. These tickets will not only cover access to the conference but also provide you with food and coffee throughout the event.

## How to Apply for Diversity Support

If you are interested in applying for diversity support tickets, we encourage you to send an email to <a href="mailto:oscal@openlabs.cc">oscal@openlabs.cc</a> with the subject line “Diversity Support for #OSCAL2023.” In your email, kindly share the reasons you believe your participation will be empowering. Rest assured that all the details you provide will be held confidential by our organizing team and will not be shared with any third parties.

## Support the Cause

If you find yourself in a position to afford a ticket, your contribution will not only support the local hackerspace in Tirana but will also enable us to extend our support to more individuals who may need it. Your purchase becomes an investment in both your own experience and in nurturing a more diverse and inclusive tech community.

<div class="box text-center">
    <a href="https://events.openlabs.cc/ol/oscal2023/" class="action-button" target="_blank">Get your Ticket</a>
</div>

Only by supporting each other in solidarity can we reevaluate the importance of open knowledge, obtain more free open-source tech, and reclaim our online privacy.
