---
title: "OSCAL is back!"
tags: "oscal"
permalink: "/blog/oscal-is-back/"
date: 2023-05-25
image: "/assets/images/OSCAL-is-Back"
imageAlt: "Attendees applauding after a talk during OSCAL"
---

We are delighted to announce that we are bringing OSCAL back on the weekend of <b>2-3 of September 2023</b>.

<!-- excerpt -->

OSCAL (Open Source Conference Albania) is the first annual conference in Albania organized to promote software freedom, open source software, free culture and open knowledge. The Conference will gather free libre open source technology users, developers, academics, governmental agencies and people who share the idea that software should be free and open for the local community and governments to develop and customize to its needs; that knowledge is a communal property and free and open to everyone.

This is the **8th edition of the conference**.

Further details and calls for participation will be announced in the coming weeks. Stay tuned! As always you can follow us on our social media:

- Mastodon [@OpenLabsAlbania](https://fosstodon.org/@OpenLabsAlbania)
- Twitter: [@OSCALconf](https://twitter.com/OSCALconf)
- Instagram: [@oscalconf](https://www.instagram.com/oscalconf/)

Contact information:

Email: <a href="mailto:oscal@openlabs.cc">oscal@openlabs.cc</a>
OSCAL is organized by Open Labs Albania, a non governmental, not for profit organization, dedicated to promote openness, freedom, transparency and decentralization by amplifying its voice as a community altogether.

Important dates

Call for sponsors on 25 of May  
Call for speakers on 31 of May

A media representative? We will be opening the call for media partners on 22 of May.

Want to help the organizing team by becoming a volunteer? 10 July
