module.exports = {
  layout: "layouts/post.html",
  eleventyComputed: {
    srcset: (data) =>
      `${data.image}-600.webp 600w, ${data.image}-999.webp 999w, ${data.image}.webp 1500w`,
    cover: (data) => `${data.image}.jpg`,
  },
};
