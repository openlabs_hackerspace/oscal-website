---
title: "Call for Sponsors: Be Part of OSCAL 2023’s Success"
permalink: "/blog/call-for-sponsors/"
date: 2023-06-29 12:00:00
image: "/assets/images/sponsors"
imageAlt: "OSCAL 2022 banner in the stage during last edition"
---

We’re thrilled to announce the Call for Sponsors for the Open Source Conference Albania (OSCAL) 2023! As one of the largest open-source events in the region, OSCAL brings together a vibrant community of developers, designers, entrepreneurs, and technology enthusiasts who are passionate about the power of free and open source software, open knowledge and open culture.

## Why Sponsor OSCAL 2023

By becoming a sponsor, you have the opportunity to:

### Showcase Your Brand

Gain exposure and increase brand awareness among a diverse audience through our comprehensive marketing campaigns, event materials, and online presence.

### Connect with the Local Open Source Industry

Network with top industry professionals, decision-makers, and potential partners during exclusive sponsor events, workshops, and dedicated networking sessions.

### Reach Your Target Audience

Gain access to a highly engaged and diverse audience of developers, designers, and technology enthusiasts who are actively seeking innovative solutions and new partnerships.

### Contribute to the Community

By sponsoring OSCAL 2023, you actively contribute to the growth and development of the local FLOSS ecosystem, supporting initiatives that promote collaboration, knowledge sharing, and innovation.

## Sponsorship Packages

There are several sponsorship packages from which to choose:

- Platinum sponsor - 5500Eur

- Gold sponsor - 2700Eur

- Silver Sponsor - 1600Eur

- Bronze sponsor- 650Eur

Download our <a href="/assets/SPONSORSHIP_PROSPECTUS_2023.pdf" target="_blank">sponsorship prospectus</a> to learn more.

## Together, We Can Make a Difference

OSCAL 2023 is more than just a conference; it’s a platform that empowers individuals and communities to make a meaningful impact through open collaboration. By sponsoring OSCAL, you’re not only investing in your brand’s success but also in the growth and development of the local open-source ecosystem.

## Contact Us Today

If you have any questions or would like to discuss customized sponsorship options, our dedicated sponsorship team is here to assist you. Reach out to us at <a href="mailto:oscal@openlabs.cc">oscal(at)openlabs.cc</a>. We look forward to partnering with you and making OSCAL 2023 an extraordinary event that leaves a lasting impact on the FLOSS community.

## Join Us as a Sponsor, Shape the Future

By sponsoring OSCAL 2023, you become an integral part of a movement that believes in the power of collaboration, innovation, and FLOSS technologies. Together, let’s shape the future of technology, inspire the next generation of innovators, and create a more inclusive and connected world. Join us on this incredible journey as a sponsor of OSCAL 2023!
