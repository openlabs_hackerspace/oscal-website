---
title: "OSCAL 2023 Tickets are now available"
tags: "oscal"
permalink: "/blog/ticket/"
date: 2023-07-29
image: "/assets/images/checkin"
imageAlt: "Oscal 2022 check-in table"
---

Are you a passionate supporter of free and open-source technology? Do you believe in the power of digital rights, open knowledge, and the public domain? If so, mark your calendars because the 2023 edition of the Open Source Conference Albania (OSCAL) is just around the corner! The event is a gathering of like-minded individuals and communities who share a common passion for all things open source. The organizing team is thrilled to announce that ticket availability for #OSCAL2023 is now open, and we can’t wait to welcome you to this inspiring conference.

## The Spirit of OSCAL: Powered by Volunteers and Supporters

OSCAL is not just another tech conference; it’s a labor of love brought to life by a group of dedicated volunteers who pour their time and passion into creating an unforgettable experience. This small but cozy gathering celebrates the values of the free libre open source movement, fostering knowledge exchange, collaboration, and innovation within the community. To keep OSCAL thriving and continue supporting the local (Open Labs) hackerspace, the conference relies on the incredible help of sponsors and supporters like you.

## Ticket Options

This year, OSCAL offers a variety of ticket options to cater to your preferences and budget. At its core, the conference aims to be accessible to all, which is why a symbolic support ticket starts at just 3Eur. This option grants you free access to presentations, workshops, information booths, and various community meetups taking place during the event.

If you want to do more to support OSCAL and its endeavors, there are three other ticket types available:

0. Pay as much as you can The “Pay as much as you can” option allows you to support OSCAL with an amount of your choice, starting from a minimum of 1 Eur. This ticket grants access to presentations, workshops, and community meetups. For those contributing more than 15Eur, complimentary coffee and food will be provided during the event. Your support makes a significant impact in sustaining and empowering the open-source community at OSCAL 2023.

1. Supporter (Early Bird): By choosing this ticket, you not only contribute to the conference’s success but also receive an extra dose of OSCAL love.

2. Supporter (Regular): This ticket level takes your support a step further. In addition to conference access, it also includes complimentary food and coffee during the event.

3. Micro-Sponsor: If you’re looking to make a significant impact, this ticket option allows you to show your support and appreciation for OSCAL. In addition to all the benefits of the previous tiers, micro-sponsors receive a special thank-you message on social media.

<div class="box text-center">
    <a href="https://events.openlabs.cc/ol/oscal2023/" class="action-button" target="_blank">Grab your Ticket</a>
</div>

## Easy Payment and Unemployment Support

To streamline the ticketing process and make it hassle-free, OSCAL accepts cash payments in-person on the day of the event at the check-in booth. This option is implemented due to difficulties associated with banks and payment gateways for non-governmental organizations in Albania.
At OSCAL, inclusivity is a top priority. If you genuinely wish to attend but are currently facing unemployment and cannot afford a ticket, fear not! Simply let the organizers know at the entrance of the venue, and they’ll be more than happy to waive the entrance fee for you. We will not share this information with anyone else.

## Supporting Local Hackerspace

Beyond attending the conference, you have the opportunity to further contribute to the community by supporting the local hackerspace in Albania through Patreon. By becoming a patron, you play a vital role in fostering the growth of open-source projects and initiatives in the region.

## Get Ready for OSCAL 2023

As OSCAL 2023 approaches, excitement is building among open-source enthusiasts from all corners of the Europe. This is your chance to immerse yourself in a thriving community, gain insights from industry experts, and collaborate on groundbreaking projects. Whether you’re a seasoned open-source advocate or just getting started, OSCAL promises an enriching experience for all.

For any inquiries or clarifications regarding ticketing or the conference, feel free to reach out to the OSCAL team at oscal@openlabs.cc. We can’t wait to see you at #OSCAL2023 and celebrate the spirit of open source together!

See you the first weekend of September :)
