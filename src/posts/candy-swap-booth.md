---
title: "Sweeten Your OSCAL Experience with the Candy Swap Booth!"
date: 2023-08-29 14:00:00
image: "/assets/images/candies"
permalink: "/blog/candy-swap-booth/"
imageAlt: "Lots of candies"
---

Get ready to add a delectable twist to your OSCAL experience this year! In addition to the exciting lineup of talks, workshops, and networking opportunities, we are thrilled to introduce a delightful new addition to the event – the Candy Swap Booth.

## The Sweet Tradition:

For the very first time at OSCAL, we’re introducing a tradition inspired by other hacker events – the Candy Swap Booth. The concept is simple and oh-so-delightful: participants from both outside Tirana and our local attendees are encouraged to bring their favorite candies to exchange with others. This adds a playful and communal touch to the conference, fostering connections and conversations that go beyond technology.

## How It Works:

Bringing together a diverse array of participants, the Candy Swap Booth is a brilliant opportunity to share a piece of your culture and personal preferences. Whether you’re bringing candies from your hometown or region, or simply showcasing your favorite treats, this exchange creates a bridge between people, enabling all participants to connect over something as universally loved as sweets.

## A Treat for Everyone:

The candy swap tradition isn’t just about the candies – it’s about sharing stories, starting conversations, and forging new friendships. While you may arrive as strangers, you’re bound to leave with a sense of camaraderie, having exchanged not just candies, but also experiences and laughter.

We owe a big thank-you to a #Hack42 member and supporter who sparked the idea for the Candy Swap Booth. Their inspiration from other hacker events has led us to create a unique experience that is sure to become a cherished part of OSCAL in the years to come.

## Join Us at OSCAL:

Are you as excited about the Candy Swap Booth as we are? If you’re eager to be a part of this vibrant exchange and the entire OSCAL experience, make sure to grab your ticket today. It’s a chance to immerse yourself in open-source culture, learn from industry experts, and now, indulge in some candy camaraderie.

<div class="box text-center">
    <a href="https://events.openlabs.cc/ol/oscal2023/" class="action-button" target="_blank">Get your Ticket</a>
</div>

Remember, OSCAL is not just a conference – it’s a celebration of community, collaboration, and the open-source spirit. Grab your ticket, pack your candies, and get ready for an experience that will leave you both inspired and with a satisfied sweet tooth!
