---
title: "Apply for Infobooth at OSCAL 2023"
date: 2023-07-18 14:00:00
image: "/assets/images/Infobooth"
permalink: "/blog/call-for-infobooths/"
imageAlt: "Infobooth table for Zextras and LibreOffice during OSCAL 2022"
---

As OSCAL is right around the corner and we gear up for an engaging and educational experience filled with insightful sessions, workshops, and networking opportunities, we invite you to be an integral part of the event by setting up an InfoBooth.

Are you interested in promoting your free libre open source project or initiative or an open data project? Is your FLOSS startup searching for new passionate developers? Is your free software/open knowledge community in search of promotion?

If your answer to one of the questions above is ‘yes’ , then you definitely should apply for an info-booth at the 8th edition of Open Source Conference Albania and spread the word that you will be at the OSCAL.

Please be aware that the topic of your info-booth MUST be related to open knowledge, open source software and/or hardware, software freedom, open data or generally open culture. You are free to propose more than one booth also, in separate submissions. For further inquiries, please feel free to contact the organizing team: <a href="mailto:oscal@openlabs.cc">oscal@openlabs.cc</a>

Take your time to fill a short application and you will have your dedicated space to promote your project/s and interact with the participants during the conference.

<div class="box text-center">
    <a 
        class="action-button" 
        href="https://cloud.openlabs.cc/apps/forms/qAcMfRaLTaTsfggj" 
        target="_blank">
        Apply
    </a>
</div>

Dates of the conference: 2-3 September 2023  
