---
title: "Check Out the Agenda for #OSCAL2023"
date: 2023-09-01 14:00:00
image: "/assets/images/agenda"
permalink: "/blog/check-out-oscal-agenda/"
imageAlt: "Badges for participants at OSCAL"
---

Hey there, fellow open source enthusiasts! We’re just a hop, skip, and a jump away from #OSCAL023, happening on the weekend of September 2nd and 3rd! As the organising team is fine tunning the latest details, let’s dive into a sneak peek of what’s waiting for us on the agenda.

## Something for Everyone

OSCAL is a relatively small, but cozy event, but with a whole bunch of stuff for everyone – no matter where you’re coming from. On Saturday, we’re cranking it up a notch with an exciting three-track lineup,and a more relaxed Sunday that features two paralel tracs. So, whether you’re a newbie or a seasoned pro, there’s something that’s just right for you.

## Talks to Spark Ideas, Workshops to Get Your Hands Dirty

The agenda is jam-packed with awesome stuff – think <strong>talks, workshops, panels, and community meetups.</strong> Workshops? Oh yeah, we’ve got 'em! These are hands-on sessions where you’ll get to roll up your sleeves and dig into cool projects. And let’s not forget the panels where we’ll chat about what’s cooking in the world of open source.

The agenda is a variety pack of open source greatness. Picture the impact of OpenStreetMap transforming mapping, the innovation of Wiki-related projects, the fascination of open metadata for books and more. Concerned about online privacy? Rest assured, we’re covering it. Intrigued by disaster recovery using open source tools? You’ll find it on the menu too.

## Recharge and Connect

Learning is awesome, but so is taking a breather. Our agenda has planned breaks for snacks and coffee. It’s not just about refueling your brain; it’s a chance to hang out, make new pals, and share stories. And don’t forget: stay hydrated during talks and workshops.

## Check the agenda

The agenda is live, and you can check out the full schedule <a href="https://cfp.openlabs.cc/oscal2023/schedule/#" target="_blank">right here.</a>
Get ready to dive into the open source spirit on the weekend of September 2nd and 3rd, 2023. Let’s celebrate the magic of working together, share knowledge and connect.

Remember, we’re all <strong>“Standing on the Shoulders of Hackers”</strong>.

See you at OSCAL 2023!

\*Stay in the loop with OSCAL updates by using #OSCAL2023 on social media (ideally Mastodon).
