---
title: "Call for Volunteers is now open!"
permalink: "/blog/oscal-2023-call-for-volunteers/"
date: 2023-06-18 12:00:00
image: "/assets/images/Volunteers"
imageAlt: "Group photo of the organizing team and volunteers during OSCAL 2022."
---

Did you know that the internet was built using free and open source software? Not only that, but Linux, Mozilla Firefox, VLC Media Player, and LibreOffice are just a few examples of Open Source software. And this is what OSCAL is all about.

Open Source Conference Albania (OSCAL) is returning to Tirana on the weekend of September 2nd-3rd for the 8th time. We offer a space for everyone who wants to use their skills and time to make OSCAL a reality, regardless of whether they have experience contributing to open source projects or would like to start.

Volunteers assist with a variety of tasks throughout the event, including participant registration, helping speakers and attendees, updating social media channels, taking pictures for social media, distributing information at the event, etc.

As a volunteer, you will collaborate closely with the small organizing team, which will direct and help you with what is needed during your activities.
Additionally, there will be at least two sessions to introduce you to the interesting duties so you may choose the one you enjoy the most.

You can devote as much volunteer time to the conference as you like, whether it be a few hours or the entire weekend, but doing so will mean you will miss part of the workshops and lectures (as there are multiple tracks), which is unavoidable.

To submit your application, kindly click the button below.

<div class="box text-center">
    <a href="https://cloud.openlabs.cc/apps/forms/97rjzCrCpYdzTc9t" class="action-button" target="_blank">Apply to Volunteer</a>
</div>
