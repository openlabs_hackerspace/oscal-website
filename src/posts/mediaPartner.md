---
title: "Call for Media Partner"
tags: "oscal"
permalink: "/blog/media-partner/"
date: 2023-07-22
image: "/assets/images/MediaPartners"
imageAlt: "Oscal 2022 Keynote presentation"
---

OSCAL presents a unique opportunity for companies, brands, and institutions from Albania, the region, and internationally to connect with young tech enthusiasts, experienced developers, hacktivists, and the next generation of the free and open-source community.

We are currently accepting applications for Media Partners for OSCAL! Whether you represent a local or international media outlet, we would be thrilled to collaborate with you and share the OSCAL experience. As we expand our reach and audience for the 8th edition of OSCAL, we need your assistance in giving the conference the recognition it deserves. By supporting us, you’ll receive support in return.

For more information, please visit our website at oscal.openlabs.cc or reach out to us at oscal(at)openlabs.cc.

Collaboration:

## What OSCAL offers your Media:

Advertisement of your website logo in promotional materials
Acknowledgement on the conference website and social media platforms
Presence during OSCAL with banners or flyers

## What Media offers OSCAL:

Advertisement of the OSCAL banner on your media website
Article(s) about the conference
Interviews opportunities.

To become a media partner, please send us an email at <a href="mailto:oscal@openlabs.cc">oscal@openlabs.cc</a>.
