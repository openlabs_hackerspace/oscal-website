---
title: "OSCAL 2023: Call for Speakers is now open!"
date: 2023-06-17 14:00:00
image: "/assets/images/Call-for-Speakers"
permalink: "/blog/call-for-speakers/"
imageAlt: "Our dear friend Lars Haefner presenting during OSCAL 2022, with the edition's poster and an Open Labs banner in the background."
---

We are pleased to announce that our call for proposals (CFP) is now open, and we would like to invite developers, speakers and promoters from Albania, the region and all around the world to submit proposals. The applicants who want to pitch at OSCAL’23 must be aware that the topic MUST be related with open knowledge, open source software and/or hardware, software freedom, online privacy or generally open culture. You can apply for either talks or workshops.

While we will most likely be having a working internet connection at the venue, please don’t rely your session solely on the Wi-Fi if possible. In this way, we can prevent unneeded technical disasters. Also, applicants are encouraged to bring their own laptops.

Almost all sessions will be held in English, so proposals should also be submitted in English. You are free to propose more than one session, in a separate submission.

This year theme is: **Standing on the Shoulders of Hackers**

Every year we choose a different theme that is reflected in the visual identity of the conference. While brainstorming, we thought that all the knowledge we have had so far has been gathered from people developing it before us. What a better way than acknowledging this and paying a homage to every one that has helped us become what we are today? It is clear that we are standing on the shoulders of other giants (hackers), and it is time to acknowledge, celebrate and be thankful to all of them!

Keep in mind this year's dates are 2-3 September. If you have any questions, please do not hesitate to contact the team at email: <a href="mailto:oscal@openlabs.cc">oscal(at)openlabs.cc</a>

<div class="box text-center">
    <a class="action-button" href="https://cfp.openlabs.cc/oscal2023/cfp" target="_blank">Apply now</a>
</div>

We can’t wait to hear your voice at OSCAL 2023!
