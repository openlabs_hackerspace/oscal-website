---
title: "OSCAL 2023 press release"
permalink: "/blog/oscal-2023-press-release/"
date: 2023-06-16 12:00:00
image: "/assets/images/Press-Release"
imageAlt: "Group photo of the participants and organizers' team during last year's edition"
---

OSCAL is back: in 2023 we are **‘Standing on Shoulders of Hackers’**

Open Source Conference Albania (OSCAL) is happening again, folks! Edition VIII will happen once again in Albania during the first weekend of September, more specifically 2nd and 3rth September in Tirana.

## About OSCAL

Open Source Conference Albania is the annual international conference in Albania organized to promote software freedom, open source software, free culture and open knowledge, a global movement that originally started more than 30 years ago. We want to believe that it is considered a well-established event in Europe gathering amazing contributors in free libre open source tech, digital commons and online privacy.

## Standing on the Shoulders of Hackers

Every year we choose a different theme that is reflected in the visual identity of the conference. While brainstorming, we thought that all the knowledge we have had so far has been gathered from people developing it before us. What a better way than acknowledging this and paying a homage to each one that has helped us become what we are today? It is clear that we are standing on the shoulders of other giants hackers, and it is time to acknowledge, celebrate and be thankful to all of you!

## What to expect

You should expect a diverse program of keynote speeches, panel discussions, workshops, and hands-on sessions conducted by free libre open source community members and long time contributors. Parallel sessions, presentations, workshops, community meetups, info-booths and social events are the elements of a standard OSCAL recipe.

We encourage all open source enthusiasts, developers, and organizations to join us at #OSCAL2023 as a participant, volunteer, speaker and/or workshop host. Whether you are a seasoned professional, a student, or simply curious about open source, digital rights or online privacy, OSCAL offers an inclusive and welcoming environment for everyone to learn, share, and connect.

## Important dates:

- Call for speakers: 17.06.2023 - 17.07.2023
- Call for volunteers: 18.06.2023 - 15.07.2023
- Call for sponsors: 19.06.2023 - 19.07.2023
- Call for media partner: 20.06.2023 - 20.07.2023
- Call for info booths: 21.06.2023 - 18.07.2023
- Venue announcement: 30 June

## About Open Labs hackerspace:

Open Labs is the Tirana-based community that promotes free libre open source tech, digital commons and online privacy culture in Albania since 2012. It is a non-government non-profit organization that also keeps the local hackerspace in Tirana running by providing a social space and infrastructure for other communities and initiatives like OpenStreetMap, Wikimedia Community User Group Albania, CryptoParty Tirana and Tirana Linux Group to grow. Community members follow a horizontal decision making process since day one.
