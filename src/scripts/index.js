// import mediaPartners from "../_data/mediaPartners";
const waitForElm = (selector) => {
  return new Promise((resolve) => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }

    const observer = new MutationObserver(() => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });

    if (document.body) {
      observer.observe(document.body, {
        childList: true,
        subtree: true,
      });
    }
  });
};

const setTheme = (theme) => {
  if (theme === "dark") {
    document.documentElement.style.setProperty("--color-background", "#070707");
    document.documentElement.style.setProperty("--color-foreground", "#fafafa");

    // hate to do this but i have to
    waitForElm(".media-partners img").then(() =>
      changeMediaPartnersLogoTheme(true)
    );
    // console.log(eleventy);
  } else if (theme === "light") {
    document.documentElement.style.setProperty("--color-foreground", "#070707");
    document.documentElement.style.setProperty("--color-background", "#fafafa");

    waitForElm(".media-partners img").then(() =>
      changeMediaPartnersLogoTheme(false)
    );
  }
};

// initialize theme as per item stored in localStorage
const theme = localStorage.getItem("oscal-theme");
setTheme(theme);

const changeMediaPartnersLogoTheme = (dark) => {
  document.querySelectorAll(".media-partners img").forEach((media) => {
    const srcset = media.getAttribute("data-src").split(",");

    if (dark) {
      media.setAttribute("src", srcset[1]);
    } else {
      media.setAttribute("src", srcset[0]);
    }
  });
};
