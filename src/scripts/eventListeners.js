window.addEventListener("load", function () {
  // toggle mobile menu with click
  document.getElementById("mobile-menu").addEventListener("click", (e) => {
    const { currentTarget } = e;
    if (currentTarget.getAttribute("aria-expanded") === "false") {
      currentTarget.setAttribute("aria-expanded", "true");
      document.body.style.position = "fixed";
    } else {
      currentTarget.setAttribute("aria-expanded", "false");
      document.body.style.position = "relative";
    }
  });

  // close the mobile menu with Escape
  document.addEventListener("keydown", (e) => {
    const { key } = e;
    if (
      key === "Escape" &&
      document.getElementById("mobile-menu").getAttribute("aria-expanded") ===
        "true"
    ) {
      document
        .getElementById("mobile-menu")
        .setAttribute("aria-expanded", "false");
      document.body.style.position = "relative";
    }
  });

  // toggle the theme
  document.getElementById("theme-toggle").addEventListener("change", (e) => {
    if (e.currentTarget.checked) {
      setTheme("dark");
      localStorage.setItem("oscal-theme", "dark");
    } else {
      setTheme("light");
      localStorage.setItem("oscal-theme", "light");
    }
  });

  // make theme toggle visible only if js is active
  document.querySelector(".theme-toggle").style.display = "block";
  // set the toggle to checked if page theme is "dark"
  if (
    theme === "dark" ||
    (!theme && window.matchMedia("(prefers-color-scheme: dark)").matches)
  ) {
    document.getElementById("theme-toggle").checked = true;
    changeMediaPartnersLogoTheme(true);
  }
});
