---
layout: "layouts/page.html"
title: "Sponsors"
permalink: "/sponsors/"
---

# Call for sponsors

<a href="/assets/SPONSORSHIP_PROSPECTUS_2023.pdf">Download the prospectus (pdf)</a>

OSCAL Organizing Committee 2022 invites you to consider sponsoring the annual conference and gathering of the open source community in Tirana. As you know our organizing team is volunteer based and despite our great desire and motivation to do the work, delivering a successful edition is also dependent upon support from our sponsors and partners.

We offer potential sponsors seven different tiers to support the event and in return we will offer exposure and presence through our communication channel. On the prospectus below you can find all the necessary information about sponsorship packages, but however if you have more questions or would like to support us on another way, please contact the organizing team at <a href="mailto:oscal@openlabs.cc">oscal(at)openlabs.cc</a>.

<br />
<br />

<h2 id="sponsors" style="text-align: center">Sponsors</h1>

<div
  class="media-partners sponsors center stack"
  style="text-align: center; --max-width: 200ch; margin-inline: -2em"
>
  <h2
    id="gold-sponsors"
    style="font-size: var(--size-step-3); max-width: unset"
  >
    Gold sponsors
  </h2>
  <div
    role="list"
    aria-labelledby="gold-sponsors"
    class="switcher"
    style="display: flex; align-items: center; --gutter: 3em; --space: 1em"
  >
    <div role="listitem" style="display: flex; justify-content: center">
      <a href="{{ site.sponsors[0].url }}" target="_blank">
        <img
          src="{{ site.sponsors[0].imageSrc }}"
          alt="{{ site.sponsors[0].label }}"
          width="400"
          height="400"
          data-src="{{ site.sponsors[0].imageSrc }}, {{ site.sponsors[0].imageSrcDark }}"
          loading="lazy"
        />
      </a>
    </div>
    <div role="listitem" style="display: flex; justify-content: center">
      <a href="{{ site.sponsors[1].url }}" target="_blank">
        <img
          src="{{ site.sponsors[1].imageSrc }}"
          alt="{{ site.sponsors[1].label }}"
          width="400"
          height="400"
          data-src="{{ site.sponsors[1].imageSrc }}, {{ site.sponsors[1].imageSrcDark }}"
          loading="lazy"
        />
      </a>
    </div>
  </div>
  <br />
  <h2 id="bronze-sponsors" style="font-size: var(--size-step-3)">
    Bronze sponsors
  </h2>
  <div role="list" aria-labelledby="bronze-sponsors">
    <div
      role="listitem"
      style="
        display: flex;
        justify-content: center;
        padding-block: 3em;
        --space: 1em;
      "
    >
      <a href="{{ site.sponsors[2].url }}" target="_blank">
        <img
          src="{{ site.sponsors[2].imageSrc }}"
          alt="{{ site.sponsors[2].label }}"
          width="250"
          height="200"
          data-src="{{ site.sponsors[2].imageSrc }}, {{ site.sponsors[2].imageSrcDark }}"
          loading="lazy"
        />
      </a>
    </div>
  </div>
</div>
