---
title: "Media"
layout: "layouts/page.html"
permalink: "/media-partners/"
---

<div class="box stack center media-page" style="--space: 2rem">
<h1 id="media-partners">Media partners</h1>

<ul class="media-partners center switcher" style="--threshold: 25rem; --s1: 0; --gutter: 3rem;" role="list" aria-labelledby="media-partners">
{% for media in site.mediaPartners %}
<li>
    <a href="{{ media.url }}" target="_blank">
    <img
        src="{{ media.imageSrc }}"
        alt="{{ media.label }}"
        width="150"
        data-src="{{ media.imageSrc }}, {{ media.imageSrcDark }}"
    />
    </a>
</li>
{% endfor %}
</ul>

This section of our website is exclusively designed for media outlets interested in promoting the eighth edition of OSCAL. Here, you can access all the essential materials, such as our logo and visual identity, along with our press releases available in both Albanian and English languages.

## Press Release

By pressing the button, you can view and download the press release of OSCAL 2020 in english.

<div class="box text-center">
    <a href="/assets/OSCAL 2023 - Press Release.pdf" class="action-button" target="_blank">Download</a>
</div>

## Deklaratë për shtyp

Duke klikuar mbi buton, ju mund të shihni dhe të shkarkoni deklaratën për shtyp.

<div class="box text-center">
    <a href="/assets/OSCAL 2023 - Deklaratë Për Shtyp.pdf" class="action-button" target="_blank">Shkarko</a>
</div>

## Apply as a media partner

Click the button below if you want to support us as a media partner.

<div class="box text-center">
    <a href="/assets/OSCAL 2023 - Call For Media Partner.pdf" class="action-button" target="_blank">Apply</a>
</div>
</div>
