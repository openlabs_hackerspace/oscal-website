---
title: "About OSCAL"
layout: "layouts/page.html"
---

# About OSCAL

OSCAL (Open Source Conference Albania) is the first annual conference in Albania organized to promote software freedom, open source software, free culture and open knowledge, a global movement that originally started more than 30 years ago.

The Conference will take place in September 2023 in Tirana and will gather free libre open source technology users, developers, academics, governmental agencies and people who share the idea that software should be free and open for the local community and governments to develop and customize to its needs; that knowledge is a communal property and free and open to everyone. Our mission is educating Albanian youth, and not only, on Free and Open Source Software, its ideology and the great community behind it.

Just like other successful regional conferences OSCAL provides a fertile ground to build and enhance networking within the tech community, both in the local region and worldwide by being used as a meeting point for a network of individuals and companies with the aim of sponsoring and initiating projects which benefit the people and taxpayers in Albania and which should also result in commercial benefits for the surrounding region.

The conference is supported and organized by Open Labs, the community that promotes free libre open source culture in Albania since 2012. Open Labs is a non-government non-profit organization, based in Albania, that aims to support and promote initiatives which provide tools that bring knowledge closer to those more in need. OSCAL: We are creating an open source ecosystem, and we want you to be part of the change!

## Agenda

Our agenda will be published around mid-august, after the submitted proposals have been reviewed and selected by our organizing team. In the meanwhile, you can get an idea of the conference structure <a href="https://wiki.openlabs.cc/faqja/OSCAL_2023/Axhenda" target="_blank">here</a>
